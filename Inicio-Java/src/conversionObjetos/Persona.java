package conversionObjetos;

public class Persona {
    protected String nombre;
    private char genero;
    protected Integer edad;
    protected String direccion;

    public Persona(String nombre, char genero){
        this.nombre = nombre;
        this.genero = genero;
    }

    public Persona(String nombre) {
    }

    @Override
    public String toString() {
        return "Persona{" +
                "nombre='" + nombre + '\'' +
                ", genero=" + genero +
                ", edad=" + edad +
                ", direccion='" + direccion + '\'' +
                '}';
    }

    public Persona(String nombre, char genero, Integer edad, String direccion) {
        this.nombre = nombre;
        this.genero = genero;
        this.edad = edad;
        this.direccion = direccion;
    }

    public char getGenero() {
        return genero;
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String metodo1(){
        return "Persona x";
    }
    public String metodoxx(){
        return "Persona x";
    }
    protected String metodoProtected(){
        return "este metodo es protected en la clase persona";
    }
    private String metodoPrivate(){
        return "este metodo es private en la clase persona";
    }
    public String metodoPublic(){
        return "este metodo es public en la clase persona";
    }
}
