package conversionObjetos;

import javax.swing.plaf.synth.SynthOptionPaneUI;
import java.sql.SQLOutput;

public class Escritor extends Empleado {
    final TipoEscritura tipoEscritura;

    public Escritor(String nombre, double sueldo, TipoEscritura tipoEscritura){
        super(nombre, sueldo );
        this.tipoEscritura = tipoEscritura;

    }
    @Override
    public String ObtenerDetalles(){
        return " ---> sobre escrito -> nombre persona: "+super.nombre+" edad:"+ super.edad+ " empleado sueldo:"+ super.sueldo + ", tipoEscritura: "+ tipoEscritura.getDescripcion();
    }

    public TipoEscritura getTipoEscritura() {
        return tipoEscritura;
    }

    public String getTipoEscrituraEnTexto() {
        return tipoEscritura.getDescripcion();
    }

    public String pruebaMetodos(){
        System.out.println( super.metodoProtected());
        System.out.println( super.metodoPublic());
        return "0";
    }




}
