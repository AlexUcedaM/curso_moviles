package conversionObjetos;

public class prueba {
    public static void main(String[] args){
        Gerente gerente= new Gerente("juan", 500.0, 45, "Finanzas", 'm');
        System.out.println("datos: ______________________________________");
        System.out.println(gerente.obtenerDetalles());
        Persona p = gerente;
        System.out.println("datos nivel 0 - persona: " + gerente.getEdad()+" genero: "+ gerente.getGenero()+
                " METODO PRIVADO:"+((Persona) p).metodo1());
        System.out.println("datos nivel 0 - nombre: " + gerente.getNombre());
        //no se puede acceder por que esta en el tercer nivel
        //System.out.println("datos nivel 0 - genero: " + gerente.getGener());

        System.out.println("datos nivel 1 - idEmpleado: " + gerente.getIdEmpleado());
        System.out.println("datos nivel 1 - sueldo: " + gerente.getSueldo());
        System.out.println("datos nivel 3: " + gerente.getDepartamento());

        Escritor escritor;
        escritor = new Escritor( "Roy", 15000, TipoEscritura.CLASICO );
        System.out.println("detalles: " + escritor.ObtenerDetalles() );
        System.out.println("----------------------------------------------");
        imprimirDetalles(escritor);

        Empleado empleado;
        empleado = new Escritor( "Roy", 1500, TipoEscritura.MODERNO);
        System.out.println("detalles: "+ empleado.ObtenerDetalles() );
        System.out.println("----------------------------------------------");
        imprimirDetalles(empleado);

        System.out.println("ACCESO A METODO PROTECTED" +  ((Persona)((Empleado) escritor)).metodoProtected());
        System.out.println("ACCESO A METODO PROTECTED" +  escritor.metodoProtected());

        System.out.println("ACCESO A METODO public" +  ((Persona)((Empleado) escritor)).metodoPublic());
        System.out.println("ACCESO A METODO public" +  escritor.metodoPublic());

        escritor.pruebaMetodos();

        Gerente gerente_1 = new Gerente("laura", 18000, 18,"sistemas", 'F' );

        Empleado emp1 = new Empleado("Juan", 10000);
        Empleado emp2 = new Empleado("Juan", 10000);

        compararObjetos(emp1,emp2);





    }

    public static void imprimirDetalles(Empleado empleado ) {
        String resultado = null;
        System.out.println("Detalle: "+ empleado.ObtenerDetalles());
        if(empleado instanceof Escritor){
            Escritor escritor = (Escritor) empleado;
            resultado = escritor.getTipoEscrituraEnTexto();
        }else if(empleado instanceof Gerente){
            resultado = ((Gerente) empleado).getDepartamento();
            System.out.println("resultado departamento: "+ resultado);
        }

    }

    public void func(){
        System.out.println("sadasd");
    }

    private static void compararObjetos(Empleado emp1, Empleado emp2){
        //Llamada metodo toString
        //Por default se manda a llamar el metodo toString dentro del sout
        System.out.println("Contenido objeto: " + emp1);

        if (emp1 == emp2){
            System.out.println("Los objetos tienen la misma direccion de memoria" );
        } else {
            System.out.println("Los objetos tienen distinta direccion de ");
        }
        if (emp1.equals(emp2)){
            System.out.println("Los objetos tenen el mismo contenido, son iguales");
        }else {

        }
    }


}
