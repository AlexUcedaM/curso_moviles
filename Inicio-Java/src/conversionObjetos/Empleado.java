package conversionObjetos;

//import fore_each.Personas;

import java.util.EmptyStackException;
import java.util.Objects;

public class Empleado extends Persona {
    private int idEmpleado;
    protected Double sueldo;
    private static int contadorEmpleados;

    private String nombreEmp;

    public Empleado(String nombre, double sueldo, Integer edad, char genero){
        super(nombre, genero);
        this.nombreEmp = nombre;
        this.sueldo = sueldo;
        this.idEmpleado = contadorEmpleados++;

    }
    @Override
    public String metodo1(){
        return "empleado z";
    }

    public Empleado(String nombre, double sueldo){
        super(nombre);
        this.nombreEmp = nombre;
        this.sueldo = sueldo;
        this.idEmpleado = contadorEmpleados++;
    }

    public int getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(int idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public double getSueldo() {
        return sueldo;
    }

    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }

    public static int getContadorEmpleados() {
        return contadorEmpleados;
    }

    public static void setContadorEmpleados(int contadorEmpleados) {
        Empleado.contadorEmpleados = contadorEmpleados;
    }


    @Override
    public String getNombre(){
        return super.getNombre()+ " desde metodo sobre-escrito";
    }
    public String ObtenerDetalles(){
        return super.toString();
    }

    public Empleado(String nombre) {
        super(nombre);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + Objects.hashCode(this.nombreEmp);
        hash = 31 * hash + (int)
                (Double.doubleToLongBits(this.sueldo) ^
                (Double.doubleToLongBits(this.sueldo)>>>32));
        return  hash;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }
        if( obj instanceof Empleado){
            Empleado emp = (Empleado) obj;
            if(nombreEmp.equals(emp.nombreEmp) && Double.valueOf(sueldo).equals(emp.sueldo)){
                return true;
            }else{
                return false;
            }
        }else {
            return false;
        }
    }

    public String getNombreEmp() {
        return nombreEmp;
    }

    public void setNombreEmp(String nombreEmp) {
        this.nombreEmp = nombreEmp;
    }

}
