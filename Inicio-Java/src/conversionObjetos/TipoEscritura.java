package conversionObjetos;

public enum TipoEscritura {
    CLASICO("escritura a mano"),MODERNO("Escritura Digital");
    String nombre;
    Float sueldo;
    String descripcion;

    TipoEscritura(String descripcion) {
        this.descripcion = descripcion;
    }

    TipoEscritura(String nombre, Float sueldo, String descripcion) {
        this.nombre = nombre;
        this.sueldo = sueldo;
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Float getSueldo() {
        return sueldo;
    }

    public void setSueldo(Float sueldo) {
        this.sueldo = sueldo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String obtenerDetalles() {
        return "TipoEscritura{" +
                "nombre='" + nombre + '\'' +
                ", sueldo=" + sueldo +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }
}
