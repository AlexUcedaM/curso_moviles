package examen.ventas;

public class Orden {
    private int idOrden;
    protected Producto [] productos= new Producto[5];

    private static int contador;

    public Orden( ){
        //this.productos = productos;
        this.idOrden = contador++;
    }

    public boolean agregarProducto(Producto producto ){
        boolean result = false;
        if(productos[productos.length-1]!=null){ //si esta llena
            System.out.println("Error: esta llena la order");
            return false;
        }else{
            for (int i = 0; i < productos.length+1 && !result; i++) {

                if(productos[i] == null){//si no esta llena
                    productos[i] = producto;
                    result=true;
                }
            }
        }
        return result;
    }

    public float calcularTotal( ){
        float total = 0;
        for (Producto p: productos) {
            if(p!=null){
                total += p.precio;
            }
        }
        return total;
    }

    public void mostarOrden(){
        for (Producto p: productos) {
            if(p != null){
                System.out.println(p.toString());
            }
        }

        System.out.println("---------------------------------");
        System.out.println("total: "+ this.calcularTotal());
        System.out.println("---------------------------------");
    }


}
