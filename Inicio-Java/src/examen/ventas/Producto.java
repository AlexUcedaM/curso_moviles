package examen.ventas;

public class Producto {
    private int idProducto=0;
    protected  String nombre="";
    protected  double precio=0;
    private static int contador;

    private static Orden orden;

    public Producto() {
        this.idProducto = contador++;
    }

    public Producto(String nombre, double precio, Orden orden) {
        this.orden = orden;
        orden.agregarProducto(this);
        this.nombre = nombre;
        this.precio = precio;
        this.idProducto = contador++;
    }
    //sobrecarga de metodo
    public Producto(String nombre, double precio) {

        this.nombre = nombre;
        this.precio = precio;
        this.idProducto = contador++;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "Producto{" +
                "idProducto=" + idProducto +
                ", nombre='" + nombre + '\'' +
                ", precio=" + precio +
                '}';
    }
}
