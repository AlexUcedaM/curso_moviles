package examen.com.gm.ventas;

import examen.ventas.Orden;
import examen.ventas.Producto;

public class Ventas {
    public static void main(String[] args) {

        Orden orden = new Orden();

        Producto producto = new Producto("producto uno", 2.0, orden);
        Producto producto2 = new Producto("producto dos", 1.0, orden);
        orden.mostarOrden();

        Producto producto3 = new Producto("producto 3", 1.0, orden);
        orden.mostarOrden();

        //probando agregar producto
        orden.agregarProducto(new Producto("producto agregado", 2.0));
        orden.agregarProducto(new Producto("producto agregado", 1.0));
        orden.agregarProducto(new Producto("producto agregado", 3.0));
        orden.mostarOrden();

    }

}
