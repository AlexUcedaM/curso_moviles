package paquetes.paquete1;

public class Clase_p1_2 {

    public Clase_p1_2() {
        //Prueba constructores
        System.out.println("");
        //Constructor publico
        new Clase_P1_1(1);
        //Constructor protegido
        new Clase_P1_1(1, 2);
        //Constructor default o package
        new Clase_P1_1(1, 2, 3);
        //Constructor private
        //new Clase1(1, 2, 3,4); 
        System.out.println("Constructor private: Acceso negado");
    }

    public void pruebaDesdeClase2() {
        //Acceso a Clase 1 desde Clase2
        Clase_P1_1 c1 = new Clase_P1_1();
        System.out.println("");
        System.out.println("Clase_p1_2 -> Clase_P1_1 publico:" + c1.atrPublico);
        System.out.println("Clase_p1_2 -> Clase_P1_1 protegido:" + c1.atrProtegido);
        System.out.println("Clase_p1_2 ->  Clase_P1_1 default:" + c1.atrPaquete);
        System.out.println("Clase_p1_2 -> Clase_P1_1 private: Acceso negado");

        System.out.println("");
        System.out.println("M Clase_p1_2 -> Clase_P1_1 publico:" + c1.metodoPublico());
        System.out.println("M Clase_p1_2 -> Clase_P1_1 protegido:" + c1.metodoProtegido());
        System.out.println("M Clase_p1_2 -> Clase_P1_1 default:" + c1.metodoPaquete());
        System.out.println("M Clase_p1_2 -> Clase_P1_1 private: Acceso negado");
    }
}
