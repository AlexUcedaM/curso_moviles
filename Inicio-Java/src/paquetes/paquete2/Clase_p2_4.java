package paquetes.paquete2;

import paquetes.paquete1.Clase_P1_1;

public class Clase_p2_4 {
    
    public Clase_p2_4(){
        //Constructor protegido, restringido al no ser una subclase
        //super(1,2);
        //Acceso package, y al estar fuera de paquete esta restringido
        //super(1,2,3);
        //Acceso privado, restringido
        //super(1,2,3,4);
    }

    public void pruebaDesdeClase4() {
        //Acceso a Clase 1 desde Clase4
        //Clase4 NO es subclase y esta en otro paquete
        Clase_P1_1 c1 = new Clase_P1_1();
        System.out.println("");
        System.out.println("Clase_p2_4 -> Clase_P1_1 publico:" + c1.atrPublico);
        System.out.println("Clase_p2_4 -> Clase_P1_1 protegido: No se puede acceder desde un paquete externo al NO ser una subclase");
        System.out.println("Clase_p2_4 -> Clase_P1_1 default: No se puede acceder desde un paquete externo");
        System.out.println("Clase_p2_4 -> Clase_P1_1 private: Acceso negado");
        
        //Constructor publico
        new Clase_P1_1();
        //los demas constructores estan restringidos

        System.out.println("");
        System.out.println("M Clase_p2_4 -> Clase_P1_1 publico:" + c1.metodoPublico());
        System.out.println("M Clase_p2_4 -> Clase_P1_1 protegido: No se puede acceder desde un paquete externo al NO ser una subclase");
        System.out.println("M Clase_p2_4 -> Clase_P1_1 default: No se puede acceder desde un paquete externo");
        System.out.println("M Clase_p2_4 -> Clase_P1_1 private: Acceso negado");
    }
}
