package paquetes.paquete2;

import paquetes.paquete1.Clase_P1_1;

public class Clase_p2_3 extends Clase_P1_1 {
    
    public Clase_p2_3(){
        //Constructor protegido, al ser una subclase puede acceder
        //aunque este en otro paquete
        super(1,2);//utilizar el constructor del heredado
        //Acceso package, y al estar fuera de paquete esta restringido
        //super(1,2,3);
        //Acceso privado, restringido
        //super(1,2,3,4);
    }

    public  void pruebaDesdeClase3() {
        //Acceso a Clase 1 desde Clase2
        //Clase 3 extiende de Clase 1
        Clase_P1_1 c1 = new Clase_P1_1();
        System.out.println("");
        System.out.println("Clase_p2_3 -> Clase_P1_1 publico:" + c1.atrPublico + " o heredado:" + atrPublico);
        System.out.println("Clase_p2_3 -> Clase_P1_1 protegido (heredado):" + atrProtegido);
        System.out.println("Clase_p2_3 -> Clase_P1_1 default: No se puede acceder desde un paquete externo" );
        System.out.println("Clase_p2_3 -> Clase_P1_1 private: Acceso negado" );
        
        //Constructor publico
        new Clase_P1_1();
        //Los demás constructores no se pueden probar asi, sino desde el constructor de esta clase
        //Ya que esta es una subclase en otro paquete
        
        System.out.println("");
        System.out.println("M Clase_p2_3 -> Clase_P1_1 publico:" + c1.metodoPublico());
        System.out.println("M Clase_p2_3 -> Clase_P1_1 protegido (heredado):" + metodoProtegido());
        System.out.println("M Clase_p2_3 -> Clase_P1_1 default: No se puede acceder desde un paquete externo");
        System.out.println("M Clase_p2_3 -> Clase_P1_1 private: Acceso negado" );
    }
}
