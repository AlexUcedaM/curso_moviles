package paquetes.modificadoresacceso;

import paquetes.paquete1.Clase_p1_2;
import paquetes.paquete2.Clase_p2_3;
import paquetes.paquete2.Clase_p2_4;

public class ModificadoresAcceso {

    public static void main(String[] args) {
        //Prueba de accesos a Clase1 desde otras clases
        //Acceso a Clase 1 desde Clase2
        System.out.println("***Acceso desde Clase 2 a Clase 1 (mismo paquete)***");
        new Clase_p1_2().pruebaDesdeClase2();

        //Acceso a Clase 1 desde Clase2
        //Clase 3 extiende de Clase 1
        System.out.println("\n***Acceso desde Clase 3 a Clase 1 (diferente paquete, pero es subclase)***");
        new Clase_p2_3().pruebaDesdeClase3();

        //Acceso a Clase 1 desde Clase4
        //Clase4 NO es subclase y esta en otro paquete
        System.out.println("\n***Acceso desde Clase 4 a Clase 1 (diferente paquete, NO es subclase)***");
        new Clase_p2_4().pruebaDesdeClase4();
    }
}
