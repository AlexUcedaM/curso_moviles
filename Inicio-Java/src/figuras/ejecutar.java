package figuras;

public class ejecutar {
    public static void main(String[] args) {
        Circulo criculo = new Circulo( );
        criculo.dibujar();

        Elipse elipse = new Elipse();
        elipse.dibujar();

        FiguraGeometrica figura = new FiguraGeometrica();
        determinaTipo(figura);

    }

    public static void determinaTodosLosTipos( FiguraGeometrica figura){
        if( figura instanceof Rectangulo){
            System.out.println("Es un rectangulo");
        }
        if( figura instanceof Triangulo){
            System.out.println("Es un triángulo");
        }
        if( figura instanceof Circulo){
            System.out.println("Es un circulo");
        }
        if( figura instanceof FiguraGeometrica){
            System.out.println("Es un FiguraGeometrica");
        }
        if( figura instanceof Object){
            System.out.println("Es un objeto");
        }else{
            System.out.println("No se encontró");
        }
    }

    public static void determinaTipo( FiguraGeometrica figura){
        if( figura instanceof Rectangulo){
            System.out.println("Es un rectangulo");
        }else
        if( figura instanceof Triangulo){
            System.out.println("Es un triángulo");
        }else
        if( figura instanceof Circulo){
            System.out.println("Es un circulo");
        }else
        if( figura instanceof FiguraGeometrica){
            System.out.println("Es un FiguraGeometrica");
        }else
        if( figura instanceof Object){
            System.out.println("Es un objeto");
        }else{
            System.out.println("No se encontró");
        }
    }
}
