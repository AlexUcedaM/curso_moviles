package oct_11_exepciones;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class prueba_errores_0 {


    public void error_divcer(){
        float l = 0;
        float k= 1;
        System.out.println( k/l );
    }
    public void error_personalizado() throws Exception {
        throw new Exception("error personalizado");
    }

    public void error_aritmetic()
    {
        try {
            int a = 30, b = 0;
            int c = a/b;  // cannot divide by zero
            System.out.println ("Result = " + c);
        }
        catch(ArithmeticException e) {
            System.out.println ("Can't divide a number by 0");
        }
    }

    public void error_puntero_null(){
        try {
            String a = null; //null value
            System.out.println(a.charAt(0));
        } catch(NullPointerException e) {
            System.out.println("NullPointerException..");
        }
    }

    public void error_indexofbound(){
        try {
            String a = "This is like chipping "; // length is 22
            char c = a.charAt(24); // accessing 25th element
            System.out.println(c);
        }
        catch(StringIndexOutOfBoundsException e) {
            System.out.println("StringIndexOutOfBoundsException");
        }
    }

    public void error_fileExeption() {
        try {

            // Following file does not exist
            File file = new File("E://file.txt");

            FileReader fr = new FileReader(file);
        } catch (FileNotFoundException e) {
            System.out.println("File does not exist");
        }
    }

    public void error_numberFormatEr(){
        try {
            // "akki" is not a number
            int num = Integer.parseInt ("akki") ;

            System.out.println(num);
        } catch(NumberFormatException e) {
            System.out.println("Number format exception");
        }
    }

}
