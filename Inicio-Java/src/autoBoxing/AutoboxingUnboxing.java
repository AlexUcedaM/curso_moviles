package autoBoxing;

public class AutoboxingUnboxing {
    private int i;
    public int j;
    int k;
    protected int l;

    public static void main(String[] args) {
        Integer enteroObj = 10;
        Float flotanteObj = 15.2f;
        Double doubleObj = 40.1;

        System.out.println("Autoboxing");
        System.out.println("Entero obj "+ enteroObj.intValue());
        System.out.println("flotante obj "+ flotanteObj.floatValue());
        System.out.println("double obj "+ doubleObj.intValue());

        int entero = enteroObj;
        float flotante= flotanteObj;
        double doble= doubleObj;

        System.out.println("Entero obj "+ entero);
        System.out.println("flotante obj "+ flotante);
        System.out.println("double obj "+ doble);
    }
}
