package fore_each;

public class Personas {

    private final int idPersona;

    String nombre;

    private static int contadorPersonas;

    public Personas( String nombre) {
        this.nombre = nombre;
        idPersona = ++contadorPersonas;
    }
    public int getIdPersona() {
        return idPersona;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getContadorPersonas() {
        return contadorPersonas;
    }

    @Override
    public String toString() {
        return "Personas{" +
                "idPersona=" + idPersona +
                ", nombre='" + nombre + '\'' +
                '}';
    }
}
