package fore_each;

import java.util.ArrayList;

public class ejecutar {
    public static void main(String[] args) {
        Personas p = new Personas("juan garcia");
        System.out.println(p.getIdPersona());
        System.out.println(p.getNombre());

        Personas[] arregloPersonas = {
                new Personas("juan"),
                new Personas("pedro")
        };

        for(Personas x : arregloPersonas){
            System.out.println("nombre de persona "+ x.getNombre()+ " - " + x.getContadorPersonas() +" - " + x.getIdPersona());
        }

        int[] edades = {15,20,41,50};

        for(int edad: edades){
            System.out.println("edad:" + edad );
        }

        ArrayList<Personas> listaPersonas = new ArrayList<Personas>();
        listaPersonas.add( new Personas("juana"));
        listaPersonas.add( new Personas("ana"));
        listaPersonas.add( new Personas("juana"));

        listaPersonas.forEach( (y) -> {
            System.out.println("nombre : " + y.getNombre() + " - " + y.getContadorPersonas());
        });


    }
}
