package herencia;

import fore_each.Personas;

public class Empleado extends Personas {
    private int idEmpleado;
    protected double sueldo;
    private static int contadorEmpleados;

    public Empleado(String nombre, double sueldo, Integer edad, char genero){
        super(nombre);

        this.sueldo = sueldo;
        this.idEmpleado = contadorEmpleados++;

    }

    public Empleado(String nombre, double sueldo){
        super(nombre);
        this.sueldo = sueldo;
        this.idEmpleado = contadorEmpleados++;
    }

    public int getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(int idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public double getSueldo() {
        return sueldo;
    }

    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }

    public static int getContadorEmpleados() {
        return contadorEmpleados;
    }

    public static void setContadorEmpleados(int contadorEmpleados) {
        Empleado.contadorEmpleados = contadorEmpleados;
    }


    @Override
    public String getNombre(){
        return super.getNombre()+ " desde metodo sobre-escrito";
    }
    public String ObtenerDetalles(){
        return super.toString();
    }
}
