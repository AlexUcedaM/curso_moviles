package herencia;

public class Gerente extends Empleado{

    public String departamento;

    public Gerente(String nombre, double sueldo, Integer edad, String departamento, char genero) {
        super(nombre, sueldo, edad, genero ); // referencia al constructor superior
        this.departamento = departamento;
    }

    public String obtenerDetalles(){
        return super.ObtenerDetalles()+ "nombre: " + this.getNombre() + " sueldo: "+
                sueldo +" departamento: "+ departamento+ " edad: "+ super.getIdPersona();
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }
}
