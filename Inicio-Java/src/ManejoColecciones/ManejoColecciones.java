package ManejoColecciones;

import java.util.*;

public class ManejoColecciones {
    public static void main(String[] args) {
        List miLista = new ArrayList();
        miLista.add("1");
        miLista.add("2");
        miLista.add("3");
        miLista.add("4");
        miLista.add("4");

        imprimir(miLista);
        Set miset = new HashSet();
        miset.add("100");
        miset.add("200");
        miset.add("300");
    }

    private static void imprimir(Collection coleccion){
        for(Object elemento: coleccion){
            System.out.println( elemento + "");
        }
        System.out.println("");
    }
}
