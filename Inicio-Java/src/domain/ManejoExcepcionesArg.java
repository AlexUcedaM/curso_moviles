package domain;

import java.util.Scanner;

public class ManejoExcepcionesArg {


    public ManejoExcepcionesArg(String args[])throws OperacionException{

        try{
            int op1 = Integer.parseInt(args[0]);
            int op2 = Integer.parseInt(args[1]);
            Divicion div = new Divicion( op1, op2);
            div.virtualizarOperacion();
        }catch (ArrayIndexOutOfBoundsException aie){
            System.out.println("ocurrio exepcion: ");
            System.out.println("hubo un error al acceder un elemento del rango");
            aie.printStackTrace();
        }catch ( NumberFormatException nfe){
            System.out.println("ocurrió una exepción");
            System.out.println("Number format exeptión");
            nfe.printStackTrace();
        }catch ( OperacionException oe){
            System.out.println("Ocurrio una exepción");
            System.out.println("Ocurrió una OperacionException");
            oe.printStackTrace();
        }finally {
            System.out.println("Ocurrio una exepción");
            System.out.println("Finalizó");
        }
    }



    public static void main (String args[])throws OperacionException{

        try{
            int op1 = Integer.parseInt(args[0]);
            int op2 = Integer.parseInt(args[1]);
            Divicion div = new Divicion( op1, op2);
            div.virtualizarOperacion();
        }catch (ArrayIndexOutOfBoundsException aie){
            System.out.println("ocurrio exepcion: ");
            System.out.println("hubo un error al acceder un elemento del rango");
            aie.printStackTrace();
        }catch ( NumberFormatException nfe){
            System.out.println("ocurrió una exepción");
            System.out.println("Number format exeptión");
            nfe.printStackTrace();
        }catch ( OperacionException oe){
            System.out.println("Ocurrio una exepción");
            System.out.println("Ocurrió una OperacionException");
            oe.printStackTrace();
        }finally {
            System.out.println("Ocurrio una exepción");
            System.out.println("Finalizó");
        }
    }
}
