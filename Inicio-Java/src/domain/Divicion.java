package domain;

public class Divicion {

    public float numerador;
    public float denominador;

    public Divicion(float numerador, float denominador) throws OperacionException {
        if(denominador==0){
           throw new OperacionException("asdasdasdasd");
        }
        this.numerador = denominador;
        this.denominador= numerador;
    }

    public void virtualizarOperacion(){
        System.out.println("el resultado de la operacion es: " + numerador/ denominador);
    }

    public float getNumerador() {
        return numerador;
    }

    public void setNumerador(float numerador) {
        this.numerador = numerador;
    }

    public float getDenominador() {
        return denominador;
    }

    public void setDenominador(float denominador) {
        this.denominador = denominador;
    }


}
