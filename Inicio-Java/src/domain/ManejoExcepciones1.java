package domain;

import javax.management.OperationsException;

public class ManejoExcepciones1 {
    public static void main(String[] args) {
        try{
            Divicion div = new Divicion(10.0F, 0F);
            div.virtualizarOperacion();
        }catch (OperacionException e) {
            throw new RuntimeException(e);
        }


    }
}
