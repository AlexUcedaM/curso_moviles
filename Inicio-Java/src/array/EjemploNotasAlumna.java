package array;

import java.util.Scanner;

public class EjemploNotasAlumna {
    public static void main(String[] args) {

        double[] claseMatematicas, claseHistoria, claseLenguaje;

        double sumNotasMatematicas = 0;double sumNotasHisotria = 0;double sumNotasLenguaje = 0;
        claseMatematicas = new double[7];
        claseHistoria = new double[7];
        claseLenguaje = new double[7];

        Scanner s = new Scanner(System.in);
        System.out.println("ingrese 7 notas de estudiantes para claseMatematicas ");
        for(int i=0; i<claseMatematicas.length; i++ ){
            claseMatematicas[i] = s.nextDouble();
        }

        System.out.println("ingrese 7 notas de estudiantes para claseHistoria ");
        for(int i=0; i<claseHistoria.length; i++ ){
            claseHistoria[i] = s.nextDouble();
        }

        System.out.println("ingrese 7 notas de estudiantes para claseLenguaje ");
        for(int i=0; i<claseLenguaje.length; i++ ){
            claseLenguaje[i] = s.nextDouble();
        }

        for (double nota : claseMatematicas) {
           //System.out.println(nota);
            sumNotasMatematicas+= nota;
        }
        for (double nota : claseLenguaje) {
            //System.out.println(nota);
            sumNotasLenguaje+= nota;
        }
        for (double nota : claseHistoria) {
            //System.out.println(nota);
            sumNotasHisotria+= nota;
        }

        System.out.println("promedios => matematicas: "+ sumNotasMatematicas/7+" historia:" + sumNotasHisotria/7 + " lenguaje:"+ sumNotasLenguaje/7);

    }
}
