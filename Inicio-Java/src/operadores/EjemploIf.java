package operadores;

public class EjemploIf {
    public static void main(String[] args) {
        int x=30;

        if( x < 20 ){
            System.out.println("X es menor que 20\n");
        }else{
            System.out.println("X mayor que 20\n");
        }

        x =10;
        if(x == 10 ){
            System.out.println("X igual a 10\n");
        }else if(x == 20 ){
            System.out.println("X igual a 20\n");
        }else if(x == 30 ){
            System.out.println("X igual a 30\n");
        }else if(x == 40 ){
            System.out.println("X igual a 40\n");
        }else{
            System.out.println("X igual a 50\n");
        }
    }
}
