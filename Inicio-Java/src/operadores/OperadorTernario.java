package operadores;

import java.util.Scanner;

public class OperadorTernario {
    public static void main(String[] args) {
        String estado = "";
        double promedio = 0.0;
        double matematicas = 0.0;
        double ciencias = 0.0;
        double historia = 0.0;


        Scanner s = new Scanner( System.in );

        System.out.print("ingrese la nota de matemáticas entre 2.0 - 7.0");
        matematicas = s.nextDouble();

        System.out.println("Ingrese la nota de ciencias entre 2.0 - 7.0 ");
        ciencias = s.nextDouble();

        System.out.println("Ingrese la nota de ciencias entre 2.0 - 7.0 ");
        historia = s.nextDouble();

        promedio = (matematicas + historia + ciencias) /3;
        System.out.println("El promedio es: "+ promedio);

        estado = promedio >= 5.49 ? "Aprovado":"Rechazado";
        System.out.println("estado: " + estado);


        /* -------------------------------- */
        //Scanner s = new Scanner(System.in);
        System.out.println("ingrese un número: ");
        int num1 = s.nextInt();

        System.out.println("ingrese un número: ");
        int num2 = s.nextInt();

        System.out.println("ingrese un número: ");
        int num3 = s.nextInt();

        System.out.println("ingrese un número: ");
        int num4 = s.nextInt();

        int max = (num1 > num2)? num1: num2;
        max = (max > num3)? max: num3;
        max = (max > num4)? max: num4;

        System.out.println("num1 = "+num1);
        System.out.println("num2 = "+num2);
        System.out.println("num3 = "+num3);
        System.out.println("num4 = "+num4);

        System.out.println("el número mayor es: " + max );

    }
}
