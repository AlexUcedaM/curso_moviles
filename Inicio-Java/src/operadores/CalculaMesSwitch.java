package operadores;

public class CalculaMesSwitch {

    public static void main(String[] args) {
        String estacion;
        String saludo = "Hola Mundo";
        int mes = 10;
        switch(mes){
            case 1: case 2: case 12:
                estacion = "Invierno";
                break;
            case 3: case 4: case 5:
                estacion = "Primavera";
                break;
            case 6: case 7: case 8:
                estacion = "Verano";
                break;
            case 9: case 10: case 11:
                estacion = "Otoño";
                break;
            default:
                estacion = "MES INCORRECTO";
                break;
        }
        System.out.println(estacion);
        if( (mes>0 && mes<3) || mes ==12 ){
            estacion = "Invierno";
        }else if( mes>2  && mes<6 ){
            estacion = "Primavera";
        }else if(mes>5 && mes<9){
            estacion = "Verano";
        }else if(mes>8 && mes<12 ){
            estacion = "Otoño";
        }else{
            estacion = "MES INCORRECTO";
        }
        System.out.println(estacion);
    }
}
