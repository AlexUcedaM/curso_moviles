package operadores;

public class SentenciaForArreglo
{
    public static void main(String[] args) {
        String[] nombres = {"andres", "pepe", "maria", "paco", "lalo", "pato", "pepa"};
        int count = nombres.length;
        for(int i =0; i<count ; i++ ){
            if( nombres[i].toLowerCase().contains( "andres".toLowerCase() ) || nombres[i].toLowerCase().contains("pepa".toLowerCase()) ){
                continue;
            }
            System.out.println(i+" "+nombres[i]);
        }
    }
}
