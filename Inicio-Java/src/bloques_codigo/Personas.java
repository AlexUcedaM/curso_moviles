package bloques_codigo;

public class Personas {


    private final int idPersona;
    private static int contadorPersonas;

    // bloque inicializacion estatico
    static {
        contadorPersonas=10;
        System.out.println("ejecutar bloque estático");
    }

    {
        System.out.println("bloque 2");
        idPersona = ++contadorPersonas;
    }

    public Personas() {
        System.out.println("crea cnstructor");
    }
    public int getIdPersona() {
        return idPersona;
    }
}
